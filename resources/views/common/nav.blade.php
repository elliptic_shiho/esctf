<div class="nav-wrapper">
  <nav class="col s12 blue darken-1 white-text">
    <a class="col s1 white-text center" href="/"> Top </a>
    <a class="col s2 white-text center" href="/scoreboard"> Scoreboard </a>
  @if (Auth::check())
    <a class="col s2 white-text center" href="/user/"> Dashboard </a>
  @if (App\User::can_make())
    <a class="col s2 white-text center" href="/challenge/"> Challenges </a>
  @endif
    <a class="col s1 white-text center right" href="/user/logout"> Logout </a>
    <a class="col white-text center right" href="/"> Logged in as {{Auth::user()->name}} </a>
  @else
    <a class="col s1 white-text center right" href="/user/login"> Login </a>
  @endif
  </nav>
</div>
