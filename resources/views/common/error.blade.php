@if (count($errors) > 0)
<script type="text/javascript">
$(document).ready(function(){
  var toastContent ="";
  @foreach ($errors->all() as $error)
    toastContent += "{{ $error }}\n";
  @endforeach
  toastContent = $('<span><i class="material-icons">error</i>' + toastContent + '</span>');
  Materialize.toast(toastContent, 5000, 'red white-text');
});
</script>
@endif
