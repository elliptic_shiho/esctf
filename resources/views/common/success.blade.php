@if (Session::has('success'))
<script type="text/javascript">
$(document).ready(function(){
  var toastContent ="";
  toastContent += "{{ Session::get('success') }}\n";
  toastContent = $('<span><i class="material-icons">done</i>' + toastContent + '</span>');
  Materialize.toast(toastContent, 5000, 'green white-text');
});
</script>
@endif
