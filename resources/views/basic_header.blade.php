<!DOCTYPE html>
<html>
  <head>
    <title>ESCTF @if (isset($esctf_title) === TRUE)  - {{$esctf_title}} @endif</title>
    <link rel="stylesheet" href="{{ URL::asset('assets/css/materialize.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/base.css') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="{{ URL::asset('assets/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/materialize.min.js') }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@include('common.error')
@include('common.success')
    <script type="text/javascript">
$.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function() {
  $('select').material_select();
  $('.modal').modal();
});
    </script>
  </head>
  <body>
    <div class="row">
      <div class="col s12">
        <h1 class="center"> ESCTF </h1>
      </div>
@include('common.nav')
