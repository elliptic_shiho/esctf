@include('basic_header')
<div class="col s12">
  <h2> Submission </h2>
  <div class="col s6 offset-s2">
    <div class="input-field">
      <label for="flag">flag</label>
      <input type="text" name="flag" id="flag">
      <button id="flag_submit" class="right btn waves-effect waves-light"> Submit </button>
    </div>
  </div>
</div>
<div class="col s12">
  <h2> Challenges </h2>
  <ul class="collapsible" data-collapsible="accordion">
    @foreach (App\Challenge::all() as $x)
      <li id="chall-{{$x->id}}"@if(App\Solve::is_solved($x)) class="green white-text"@endif>
        <div class="collapsible-header @if(App\Solve::is_solved($x)) green white-text @endif"><i class="material-icons">{{ App\Genre::to_icon($x->genre) }}</i>{{$x->name}} ({{$x->point}}pts)</div>
        <div class="collapsible-body container"><pre>{{$x->description}}</pre></div>
      </li>
    @endforeach
  </ul>
</div>
<script>
$(document).ready(function() {
  $(".collapsible").collapsible();
  $("#flag_submit").click(function() {
    $.ajax({
      type: "POST",
      url: "/flag/submit",
      data: "flag=" + encodeURIComponent($("#flag").val()),
      success: function (d) {
        var json = JSON.parse(d);
        if (json["result"] === 1) {
          Materialize.toast($('<span><i class="material-icons">done</i>You got ' + json['point'] + 'pt, Congratulations!</span>'), 5000, 'green white-text');
          $("#chall-" + json["challid"]).addClass("green white-text");
          $("#chall-" + json["challid"] + " .collapsible-header").addClass("green white-text");
        } else if (json["result"] === 2) {
          Materialize.toast($('<span>You already solved that challenge.</span>'), 5000);
        } else if (json["result"] === 0) {
          msg = ["Try harder!", "This is not a flag.", "Where is flag?", "Nope...", "Incorrect"];
          Materialize.toast($('<span>' + msg[Math.floor(Math.random() * msg.length)] + '</span>'), 5000, 'red white-text');
        } else if (json["result"] === 3) {
          Materialize.toast($('<span><i class="material-icons">done</i>You got ' + json['point'] + 'pt, but you weren\'t logging in!</span>'), 5000, 'green white-text');
        }
      }
    });
  });
});
</script>
@include('basic_footer')
