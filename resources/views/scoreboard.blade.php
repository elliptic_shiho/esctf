@include('basic_header')
<div class="col s12">
  <h2> Scoreboard </h2>
  <div class="container">
  <table class="highlight">
    <thead>
      <tr>
        <th data-field="rank" class="center">Rank</th>
        <th data-field="user">User</th>
        <th data-field="score" class="center">Score</th>
      </tr>
    </thead>
    <tbody>
  @foreach ($data as $x)
      <tr>
        <th class="center">{{$x["rank"]}}</th>
        <th><img width="25" src="http://www.gravatar.com/avatar/{{ md5($x["email"]) }}"> {{$x["name"]}}</th>
        <th class="center">{{$x["score"]}}</th>
      </tr>
    @endforeach
    </tbody>
  </table>
  </div>
</div>
@include('basic_footer')
