@include('basic_header', ['esctf_title' => 'Challenges' ])
<div class="col s12">
  <ul class="collection">
    @foreach (App\Challenge::all() as $x)
    <li class="collection-item avatar">
      <i class="material-icons circle">{{App\Genre::to_icon($x->genre)}}</i>
      <span class="title">
        <a href="/challenge/edit/{{$x->id}}">{{$x->name}}</a>
        <p><pre>{{$x->description}}</pre></p>
      </span>
      <a href="#" data-id="{{$x->id}}" class="delete_modal secondary-content"><i class="material-icons">delete</i></a>
    </li>
    @endforeach
  </ul>
</div>

<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
  <a href="/challenge/create" class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">add</i></a>
</div>

<div id="modal_delete_confirm" class="modal">
  <div class="modal-content">
    <h4> Delete Challenge </h4>
    <p> Are you sure? </p>
  <div>
  <div class="modal-footer row">
    <div class="col offset-s3 s3">
      <a href="#" id="modal_ok_btn" class="modal_action modal_close waves_effect waves-light btn-flat red white-text col s12 center">OK</a>
    </div>
    <div class="col s3">
      <a href="#" id="modal_cancel_btn" class="modal_action modal_close waves_effect waves-light btn-flat green white-text col s12 center">Cancel</a>
    </div>
  </div>
</div>
<script>
$(function(){
  (function() {
  var modal_deferred = null;
    $('#modal_ok_btn').click(function() {
      modal_deferred.resolve();
      $('');
    });
    $('#modal_cancel_btn').click(function() {
      modal_deferred.reject();
    });
    $('.delete_modal').click(function() {
      var id = $(this).data('id');
      modal_deferred = new $.Deferred();
      modal_deferred.promise().done(function() {
        $.ajax({
          type: "POST",
          url: "/challenge/delete",
          data: "id=" + id,
          success: function (d) {
            if (d["status"] === "error") {
              Materialize.toast('Error: ' + d["reason"], 5000, 'red')
            } else if (d["status"] === "success") {
              location.reload(true);
            }
          }
        });
      }).always(function(){
        $('#modal_delete_confirm').modal('close');
      });
      $('#modal_delete_confirm').modal('open');
    });
  })();
});
</script>
@include('basic_footer')
