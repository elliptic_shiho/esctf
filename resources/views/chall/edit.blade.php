@include('basic_header', ['esctf_title' => "Edit Challenge - " . $chall->name])
<div class="col s12">
  <h2> Editing {{ $chall->name }} </h2>
  <div class="container center row">
    <form action="/challenge/edit" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="ID" value="{{ $chall->id }}"/>
      <div class="input-field col s6">
        <input id="chall_name" type="text" name="name" value="{{ $chall->name }}"/>
        <label for="chall_name">Challenge Name</label>
      </div>
      <div class="input-field col s4">
        <input type="number" id="chall_point" name="point" value="{{ $chall->point }}"/>
        <label for="chall_point">Challenge Point</label>
      </div>
      <div class="input-field col s8">
        <input type="text" id="chall_flag" name="flag" value=""/>
        <label for="chall_flag">Challenge Flag</label>
      </div>
      <div class="input-field col s4">
        <select name="genre" id="chall_genre">
        @foreach (App\Genre::all() as $x)
          <option value="{{$x->id}}" @if($chall->genre == $x->id) selected @endif>{{$x->name}}</option>
        @endforeach
        </select>
        <label for="chall_genre">Challenge Genre</label>
      </div>
      <div class="input-field col s12">
        <textarea name="description" class="materialize-textarea" id="chall_description">{{ $chall->description }}</textarea>
        <label for="chall_description">Challenge Description</label>
      </div>
      <div class="col s3 offset-s9">
        <button class="btn waves-effect waves-light" type="submit">
          submit
          <i class="material-icons right">send</i>
        </button>
      </div>
    </form>
  </div>
</div>
@include('basic_footer')
