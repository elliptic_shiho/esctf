@include('basic_header', ['esctf_title' => $chall->name ])
<div class="col s12">
  <div class="container center">
    <h2> {{ $chall->name }} ({{App\Genre::find($chall->genre)->name}} {{$chall->point}}pts)</h2>
    <pre class="left-align challenge_description">
{{ $chall->description }}
    </pre>
  </div>
</div>
@include('basic_footer')
