@include('basic_header', ['esctf_title' => 'Register'])
<div class="col s12">
  <h3> Register: </h3>
  <form method="post" action="/user/register">
    {{ csrf_field() }}
    <div class="input-field col s6 offset-s2">
      <input type="text" name="name" id="name">
      <label for="nae">Name</label>
    </div>
    <div class="input-field col s6 offset-s2">
      <input type="email" name="email" id="email" class="validate">
      <label for="email" data-error="Wrong" data-success="OK">E-Mail</label>
    </div>
    <div class="input-field col s6 offset-s2">
      <input type="password" name="password" id="password">
      <label for="password">Password</label>
    </div>
    <div class="input-field col s6 offset-s2">
      <input type="password" name="re_password" id="re-password">
      <label for="re-password">Re-type Password</label>
    </div>
    <div class="input-field col s6 offset-s2">
      <button class="btn waves-effect waves-light" type="submit"> Resigter </button>
    </div>
  </form>
</div>
@include('basic_footer')
