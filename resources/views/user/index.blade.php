@include('basic_header', ['esctf_title' => 'Dashboard'])
<div class="col s12 container">
  <h3>Solved challenges</h3>
  <p> You solved {{ count($challs) }} challenge(s), and you have {{ $sum }} point(s). </p>
  <table>
    <thead>
      <tr>
        <th data-field="solve_name">Challenge Name</th>
        <th data-field="solve_point">Point</th>
        <th data-field="solve_time">Solve time</th>
      </tr>
    </thead>
    <tbody>
  @foreach ($challs as $x)
    <tr><th>{{ $x->name }}</th><th>{{ $x->point }}</th><th>{{ $x->updated_at }}</th></tr>
  @endforeach
  </ul>
</div>
@include('basic_footer')

