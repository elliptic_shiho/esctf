@include('basic_header', ['esctf_title' => 'profile'])
<div class="col s12">
  <h3> Login: </h3>
  <form method="post" action="/user/login">
    {{ csrf_field() }}
    <div class="input-field col s6 offset-s2">
      <input type="email" name="email" id="email" class="validate">
      <label for="email" data-error="Wrong" data-success="OK">E-Mail</label>
    </div>
    <div class="input-field col s6 offset-s2">
      <input type="password" name="password" id="password">
      <label for="password">Password</label>
    </div>
    <div class="input-field col s6 offset-s2">
      <button class="btn waves-effect waves-light" type="submit"> Login <i class="material-icons right">send</i> </button>
    </div>
    <div class="col s6 offset-s2">
      <p>
        Haven't account? Let's <a href="/user/register">join</a> to the CTF!
      </p>
    </div>
  </form>
</div>
@include('basic_footer')
