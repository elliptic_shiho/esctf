<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Solve extends Model
{
  protected $table = 'solves';

  public static function is_solved($chall) {
    if (Auth::check()) {
      return Solve::where('user', Auth::user()->id)->where('problem', $chall->id)->count() === 1;
    } else {
      return false;
    }
  }
}
