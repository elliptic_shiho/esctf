<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

Route::controller('/user', 'UserController');

Route::controller('/challenge', 'ChallengeController');

Route::post('/flag/submit', 'FlagController@submit');

Route::get('/scoreboard', 'MiscController@scoreboard');
