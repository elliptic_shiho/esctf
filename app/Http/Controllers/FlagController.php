<?php

namespace App\Http\Controllers;

use Auth;
use App\Challenge;
use App\Solve;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FlagController extends Controller {
  public function submit(Request $req) {
    $res = array();
    $send = $req->flag;
    $res["result"] = 0;
    foreach (Challenge::all() as $x) {
      if (password_verify($send, $x->flag)) { // AC
        if (Auth::check()) {
          if (Solve::where('user', Auth::user()->id)->where('problem', $x->id)->count() !== 0) {
            $res["result"] = 2;
          } else {
            $res["result"] = 1;
            $res["point"] = $x->point;
            $res["challid"] = $x->id;
            $s = new Solve();
            $s->user = Auth::user()->id;
            $s->problem = $x->id;
            $s->save();
          }
        } else {
          $res["result"] = 3;
          $res["point"] = $x->point;
          $res["challid"] = $x->id;
        }
        break;
      }
    }
    return json_encode($res);
  }
}
