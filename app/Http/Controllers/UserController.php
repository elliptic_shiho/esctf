<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Challenge;
use App\Solve;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller {
  use AuthenticatesAndRegistersUsers, ThrottlesLogins;

  public function getIndex() {
    if (Auth::check()) {
      $solves = Solve::where('user', Auth::user()->id)->get();
      $problems = array();
      $sum = 0;
      foreach ($solves as $x) {
        $chall = Challenge::find($x["problem"]);
        if ($chall !== null) {
          $problems[] = $chall;
          $sum += $chall->point;
        }
      }
      return view('user/index', ['solves' => $solves, 'challs' => $problems, 'sum' => $sum]);
    } else {
      return redirect()->action('UserController@getLogin');
    }
  }

  public function getLogin() {
    return view('user/login');
  }

  public function postLogin(Request $req) {
    $validator = Validator::make($req->all(), [
      'email' => 'required|email|max:255',
    ]);

    if ($validator->fails()) {
      return redirect()->action('UserController@getLogin')->withInput()->withErrors($validator);
    }
    if (Auth::attempt(['email' => $req->email, 'password' => $req->password])) {
      return redirect()->action('UserController@getIndex')->with('success', 'Success Logged In.');
    } else {
      return redirect()->action('UserController@getLogin')->withInput()->withErrors(array('ID' => 'Invalid email or password.'));
    }
  }

  public function getRegister() {
    if (Auth::check()) {
      return redirect()->action('UserController@getIndex');
    } else {
      return view('user/register');
    }
  }

  public function postRegister(Request $req) {
    $validator = Validator::make($req->all(), [
      'email' => 'required|email|max:255|unique:users',
      'name' => 'required|max:255|unique:users',
    ]);

    if ($validator->fails()) {
      return redirect()->action('UserController@getRegister')->withInput()->withErrors($validator);
    }
    if ($req->password !== $req->re_password) {
      return redirect()->action('UserController@getRegister')->withErrors(array('ID' => 'Password mismatch'));
    }

    $user = User::create([
          'name' => $req->name,
          'email' => $req->email,
          'password' => bcrypt($req->password),
          'can_make' => false
    ]);
    $user->save();
    Auth::attempt(['email' => $req->email, 'password' => $req->password]);

    return redirect()->action('UserController@getIndex')->with('success', 'Registration Successful.');
  }
}
