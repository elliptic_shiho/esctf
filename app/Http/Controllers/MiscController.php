<?php

namespace App\Http\Controllers;

use App\Challenge;
use App\Solve;
use App\User;

use App\Http\Controllers\Controller;

class MiscController extends Controller {
  public function scoreboard() {
    $data = array();
    foreach (User::all() as $user) {
      $solves = Solve::where('user', $user->id)->get();
      $sum = 0;
      foreach($solves as $solve) {
        $chall = Challenge::find($solve->problem);
        $sum += $chall["point"];
      }
      $data[] = array("name"=>$user["name"], "email"=>$user["email"], "score"=>$sum);
    }
    array_multisort(array_column($data, 'score'), SORT_DESC, SORT_NUMERIC, $data);
    $prev = 0;
    $rank = 1;
    if (count($data) > 0) {
      $prev = $data[0]["score"];
      for($i = 0; $i < count($data); $i++) {
        if ($prev !== $data[$i]["score"]) {
          $rank++;
          $prev = $data[$i]["score"];
        }
        $data[$i]["rank"] = $rank;
      }
    }
    return view('scoreboard', ['data' => $data]);
  }
}
