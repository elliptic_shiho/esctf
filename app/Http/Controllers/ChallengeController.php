<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Challenge;
use App\Solve;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ChallengeController extends Controller {
  use AuthenticatesAndRegistersUsers, ThrottlesLogins;

  public function getIndex() {
    if (User::can_make()) {
      return view('chall/index');
    } else {
      return redirect('/')->withErrors(array('ID' => 'Operation not permitted'));
    }
  }

  public function getEdit(Request $req) {
    if (User::can_make()) {
      $path = explode('/', $req->path());
      $chall_id = (int)(end($path));
      $chall = Challenge::where(['id' => $chall_id]);
      if ($chall->count() != 1) {
        return redirect('/challenge/')->withErrors(array('ID' => 'Invalid Challenge ID'));
      }
      return view('chall/edit', ['chall' => $chall->get()[0]]);
    } else {
      return redirect('/')->withErrors(array('ID' => 'Operation not permitted'));
    }
  }

  public function postEdit(Request $req) {
    if (User::can_make()) {
      $chall = Challenge::where(['id' => $req->ID]);
      if ($chall->count() != 1) {
        return redirect('/challenge/')->withErrors(array('ID' => 'Invalid Challenge ID'));
      }
      $chall = $chall->get()[0];
      if ($req->name !== $chall->name) {
        $chall->name = $req->name;
      }
      if ($req->flag !== '') {
        $chall->flag = password_hash($req->flag, PASSWORD_DEFAULT);
      }
      if ($req->description !== $chall->description) {
        $chall->description = $req->description;
      }
      if ($req->point !== $chall->point) {
        $chall->point = $req->point;
      }
      if ($req->genre !== $chall->genre) {
        $chall->genre = $req->genre;
      }
      $chall->save();
      return redirect('/challenge/')->with('success', 'Success');
    } else {
      return redirect('/')->withErrors(array('ID' => 'Operation not permitted'));
    }
  }

  public function getCreate(Request $req) {
    if (User::can_make()) {
      return view('chall/create');
    } else {
      return redirect('/')->withErrors(array('ID' => 'Operation not permitted'));
    }
  }

  public function postCreate(Request $req) {
    if (User::can_make()) {
      $chall = new Challenge();
      $chall->name = $req->name;
      $chall->flag = password_hash($req->flag, PASSWORD_DEFAULT);
      $chall->description = $req->description;
      $chall->point = $req->point;
      $chall->genre = $req->genre;
      $chall->save();
      return redirect('/challenge/')->with('success', 'Success');
    } else {
      return redirect('/')->withErrors(array('ID' => 'Operation not permitted'));
    }
  }

  public function postDelete(Request $req) {
    if (User::can_make()) {
      $chall = Challenge::where(['id' => $req->id]);
      if ($chall->count() !== 1) {
        return response()->json([
          'status' => 'error',
          'reason' => 'Invalid Challenge ID'
        ]);
      }
      $chall = $chall->get()[0];
      $chall->delete();
      return response()->json([
        'status' => 'success',
      ]);
    } else {
      return response()->json([
        'status' => 'error',
        'reason' => 'Operation not permitted'
      ]);
    }
  }
}
