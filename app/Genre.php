<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
  protected $table = 'genres';

  public static function to_icon($id) {
    return Genre::find($id)->icon;
  }
}
